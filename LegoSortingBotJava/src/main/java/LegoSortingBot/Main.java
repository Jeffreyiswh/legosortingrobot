package LegoSortingBot;

import LegoSortingBot.Listeners.gestureListener;
import LegoSortingBot.windows.WindowMain;

import java.io.IOException;

public class Main {

    public static WindowMain windowMain;

    public static void main(String[] args) throws IOException, InterruptedException {

        windowMain = new WindowMain();
        gestureListener gestListener = new gestureListener();
        gestListener.initialize(windowMain);
        Thread t = new Thread() {
            public void run() {
                //the following line will keep this app alive for 1000 seconds,
                //waiting for events to occur and responding to them (printing incoming messages to console).
                try {
                    Thread.sleep(1000000);
                } catch (InterruptedException ie) {
                }
            }
        };
        t.start();

        System.out.println("Started");
    }

    public WindowMain getWindowMain() { return windowMain; }
}