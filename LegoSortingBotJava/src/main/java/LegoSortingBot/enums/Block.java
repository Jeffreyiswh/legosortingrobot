package LegoSortingBot.enums;

import java.awt.*;

public enum Block {
    //name|format|color|kind|box
    NORMALE_RED_4("NRED4", 4, Color.red, "NORMALE", Box.NRED4),
    NORMALE_BLUE_6("NBLUE6", 6, Color.blue, "NORMALE", Box.NBLUE6);

    private final String name;
    private final int format;
    private final Color color;
    private final String kind;
    private final Box box;

    private Block(String name, int format, Color color, String kind, Box box){
        this.name = name;
        this.format = format;
        this.color = color;
        this.kind = kind;
        this.box = box;
    }

    public static Block getBlock(String name){
        Block result = null;
        for(Block block : Block.values()) {
            if(block.getName().equals(name)) { result = block; }
        }
        return result;
    }

    public String getName() { return name; }

    public int getFormat() { return format; }

    public Color getColor() { return color; }

    public String getKind() { return kind; }

    public Box getBox() { return box; }
}
