package LegoSortingBot.enums;

public enum Box {
    //name|block|PathTo|ServoFrom
    NRED4("B_NRED4", Block.NORMALE_RED_4, new Servo[] {Servo.HOOFD_VERDEEL_I}, Servo.EXIT_NRED4),
    NBLUE6("B_NBLUE6", Block.NORMALE_BLUE_6, new Servo[] {}, Servo.EXIT_NBLUE6);

    private final String name;
    private final Block block;
    private final Servo[] pathto;
    private final Servo servofrom;

    private Box(String name, Block block, Servo[] pathto, Servo servofrom){
        this.name = name;
        this.block = block;
        this.pathto = pathto;
        this.servofrom = servofrom;
    }

    public static Box getBox(String name){
        Box result = null;
        for(Box box : Box.values()) {
            if(box.getName().equals(name)) { result = box; }
        }
        return result;
    }

    public String getName() { return name; }

    public Block getBlock() { return block; }

    public Servo[] getPathto() { return pathto; }

    public final Servo getServofrom() { return servofrom; }

}
