package LegoSortingBot.enums;

public enum Servo {
        //name|pin|angle
        HOOFD_VERDEEL_I("HV1", 23, 65, 110),
        EXIT_NRED4("ENRED4", 18, 55, 0),
        EXIT_NBLUE6("ENBLUE6", 15, 45, 0);


        private final String name;
        private final int pin;
        private final int activeangle;
        private final int sleepangle;

        private Servo(String name, int pin, int activeangle, int sleepangle) {
            this.name = name;
            this.pin = pin;
            this.activeangle = activeangle;
            this.sleepangle = sleepangle;
        }

        public static Servo getServo(String name) {
            Servo result = null;
            for(Servo servo : Servo.values()) {
                if(servo.getName().equals(name)) { result = servo; }
            }
            return result;
        }

        public final String getName() {
            return name;
        }

        public final int getPin() { return pin; }

        public final int getActiveangle() { return activeangle; }

        public final int getSleepangle() { return sleepangle; }
    }


