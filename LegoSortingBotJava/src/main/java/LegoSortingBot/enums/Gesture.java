package LegoSortingBot.enums;

public enum Gesture {
        //tag|input
        NORTH_SOUTH("GEST_NS", "Gesture North to South"),
        SOUTH_NORTH("GEST_SN", "Gesture South to North"),
        WEST_EAST("GEST_WE", "Gesture West to East"),
        EAST_WEST("GEST_EW", "Gesture East to West"),
        CENTER("GEST_CENTRE", "Touch Centre");


        private final String tag;
        private final String input;

        private Gesture(String tag, String input) {
            this.tag = tag;
            this.input = input;
        }

        public static Gesture getGesture(String input) {
            Gesture result = null;
            for(Gesture gesture : Gesture.values()) {
                if(gesture.getInput().equals(input)) { result = gesture; }
            }
            return result;
        }

        public final String getTag() { return tag; }

        public final String getInput() { return input; }

}
