package LegoSortingBot.windows.windows;

import LegoSortingBot.enums.Block;
import LegoSortingBot.enums.Gesture;
import LegoSortingBot.executers.GuideProcessor;
import LegoSortingBot.windows.WindowMain;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class GuideProcesWindow extends Window{
    private boolean isActive = false;

    private WindowMain windowMain;
    private GuideProcessor guideProcessor;
    private JFileChooser fileChooser;

    private JButton openButton;

    public GuideProcesWindow(WindowMain windowMain){
        super("guide_proces");
        this.windowMain = windowMain;
        guideProcessor = windowMain.getGuideProcessor();
    }

    @Override
    public JPanel getPanel(){
        JPanel content = new JPanel();

        GridLayout gridLayout = new GridLayout(11,3,0,0);
        content.setLayout(gridLayout);

        List<Block> currentStep = guideProcessor.getCurrentStep();
        StringBuilder step = new StringBuilder();
        for(int i=0;i<currentStep.size();i++){
            step.append(currentStep.get(i).getName() + " ");
        }

        content.add(new JLabel());
        content.add(new JLabel("Handleiding Modus"));
        content.add(new JLabel());

        content.add(new JSeparator());
        content.add(new JSeparator());
        content.add(new JSeparator());

        content.add(new JLabel());
        content.add(new JLabel(guideProcessor.getError()));
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel("Block's in deze stap (stap "+ (guideProcessor.getCurrStep()+1) + "): "));
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel(step.toString()));
        content.add(new JLabel());

        content.add(new JLabel("Veeg naar links voor de vorige stap"));
        content.add(new JLabel());
        content.add(new JLabel("Veeg naar rechts voor de volgende stap"));

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JSeparator());
        content.add(new JSeparator());
        content.add(new JSeparator());

        content.add(new JLabel());
        content.add(new JLabel("Veeg omlaag om terug te gaan naar Home!"));
        content.add(new JLabel());


        return content;
    }

    @Override
    public void handleGesture(Gesture gesture){
        if(gesture.equals(Gesture.SOUTH_NORTH)){
            windowMain.setWindow(windowMain.getHomeWindow());
        } else if(gesture.equals(Gesture.WEST_EAST)){
            guideProcessor.nextStep();
        } else if(gesture.equals(Gesture.EAST_WEST)){
            guideProcessor.lastStep();
        }
    }

    @Override
    public boolean IsActive(){ return isActive; }

    @Override
    public void setActive(Boolean newActive) { isActive = newActive; }

}
