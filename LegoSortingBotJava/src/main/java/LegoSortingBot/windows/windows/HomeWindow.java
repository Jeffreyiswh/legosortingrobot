package LegoSortingBot.windows.windows;

import LegoSortingBot.enums.Gesture;
import LegoSortingBot.windows.WindowMain;
import com.sun.org.apache.xpath.internal.operations.Bool;

import javax.swing.*;
import java.awt.*;

public class HomeWindow extends Window {
    private boolean isActive = false;
    private WindowMain windowMain;

    public HomeWindow(WindowMain windowMain){
        super("home");
        this.windowMain = windowMain;
    }

    @Override
    public JPanel getPanel(){
        JPanel content = new JPanel();

        GridLayout gridLayout = new GridLayout(11,3,0,0);
        content.setLayout(gridLayout);

        JLabel mainLabel = new JLabel("Welkom! Swipe om verder te gaan");
        JLabel topLabel = new JLabel("Veeg omhoog voor de handmatige Modus");

        content.add(new JLabel());
        content.add(topLabel);
        content.add(new JLabel());

        content.add(new JSeparator());
        content.add(new JSeparator());
        content.add(new JSeparator());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel("Veeg naar links voor de sorteer modus"));
        content.add(mainLabel);
        content.add(new JLabel("Veeg naar rechts voor de handleiding modus"));

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JSeparator());
        content.add(new JSeparator());
        content.add(new JSeparator());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());
        return content;
    }

    @Override
    public void handleGesture(Gesture gesture){
        if(gesture.equals(Gesture.NORTH_SOUTH)){
            windowMain.setWindow(windowMain.getManualWindow());
        } else if(gesture.equals(Gesture.WEST_EAST)){
            windowMain.setWindow(windowMain.getGuideWindow());
        } else if(gesture.equals(Gesture.EAST_WEST)){
            windowMain.setWindow(windowMain.getSortWindow());
        }
    }

    @Override
    public boolean IsActive(){ return isActive; }

    @Override
    public void setActive(Boolean newActive) { isActive = newActive; }
}
