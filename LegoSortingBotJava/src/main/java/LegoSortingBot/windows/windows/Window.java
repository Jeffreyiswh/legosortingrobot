package LegoSortingBot.windows.windows;

import LegoSortingBot.enums.Gesture;

import javax.swing.*;

public abstract class Window {
    private String title;

    public Window(String title){
        this.title = title;
    }

    public abstract void handleGesture(Gesture gesture);
    public abstract JPanel getPanel();
    public abstract boolean IsActive();
    public abstract void setActive(Boolean newActive);

    public String getTitle(){ return title; }
}
