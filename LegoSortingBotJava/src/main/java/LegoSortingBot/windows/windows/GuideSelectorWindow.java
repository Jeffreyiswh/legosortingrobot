package LegoSortingBot.windows.windows;

import LegoSortingBot.enums.Gesture;
import LegoSortingBot.executers.GuideProcessor;
import LegoSortingBot.windows.WindowMain;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static jdk.nashorn.internal.runtime.regexp.joni.Config.log;

public class GuideSelectorWindow extends Window implements ActionListener{
    private boolean isActive = false;

    private WindowMain windowMain;
    private GuideProcessor guideProcessor;
    private JFileChooser fileChooser;

    private JButton openButton;

    public GuideSelectorWindow(WindowMain windowMain){
        super("guide_selector");
        this.windowMain = windowMain;
        guideProcessor = windowMain.getGuideProcessor();
    }

    @Override
    public JPanel getPanel(){
        JPanel content = new JPanel();

        JTextArea log = new JTextArea(5,20);
        log.setMargin(new Insets(5,5,5,5));
        log.setEditable(false);

        JScrollPane logScrollPane = new JScrollPane(log);

        fileChooser = new JFileChooser();

        openButton = new JButton("Open");
        openButton.addActionListener(this);

        content.add(new JLabel("Selecteert u alstublieft een bestand met uw muis."), BorderLayout.NORTH);
        content.add(openButton, BorderLayout.PAGE_START);
        content.add(logScrollPane, BorderLayout.CENTER);
        content.add(new JSeparator(), BorderLayout.SOUTH);
        content.add(new JLabel("Veeg omlaag om terug te gaan naar home"), BorderLayout.PAGE_END);

        return content;
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == openButton){
            int returnVal = fileChooser.showOpenDialog(windowMain.getActivePanel());

            if(returnVal == JFileChooser.APPROVE_OPTION){
                String filePath = fileChooser.getSelectedFile().getPath();
                guideProcessor.loadGuide(fileChooser.getSelectedFile().getPath());
                System.out.println("Loading guide file");
            }
        }
    }

    @Override
    public void handleGesture(Gesture gesture){
        if(gesture.equals(Gesture.SOUTH_NORTH)){
            windowMain.setWindow(windowMain.getHomeWindow());
        }
    }

    @Override
    public boolean IsActive(){ return isActive; }

    @Override
    public void setActive(Boolean newActive) { isActive = newActive; }

}
