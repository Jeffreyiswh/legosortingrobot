package LegoSortingBot.windows.windows;

import LegoSortingBot.enums.Block;
import LegoSortingBot.enums.Gesture;
import LegoSortingBot.executers.PlaceExecuter;
import LegoSortingBot.windows.WindowMain;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SortWindow extends Window {
    private boolean isActive = false;
    private WindowMain windowMain;

    private List<Block> blocks = new ArrayList<Block>();
    private Block currBlock;
    private Integer currBlockNum = 0;

    private String next;
    private String nextDefault = "Veeg naar rechts voor het volgende block";

    private String last;
    private String lastDefault = "Veeg naar links voor het vorige block";

    public SortWindow(WindowMain windowMain){
        super("sort");
        this.windowMain = windowMain;

        for(Block block: Block.values()){
            blocks.add(block);
        }

        currBlock = blocks.get(0);

        if(blocks.size() > 1){
            next = nextDefault;
        }
    }

    @Override
    public JPanel getPanel(){
        JPanel content = new JPanel();
        GridLayout gridLayout = new GridLayout(11,3,0,0);
        content.setLayout(gridLayout);

        content.add(new JLabel());
        content.add(new JLabel("Veeg omhoog om het pad voor 6 sec in te stellen"));
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JSeparator());
        content.add(new JSeparator());
        content.add(new JSeparator());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel("Block:"));
        content.add(new JLabel());

        content.add(new JLabel(last));
        content.add(new JLabel(currBlock.getName()));
        content.add(new JLabel(next));

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JSeparator());
        content.add(new JSeparator());
        content.add(new JSeparator());

        content.add(new JLabel());
        content.add(new JLabel("Veeg omlaag om terug naar Home te gaan!"));
        content.add(new JLabel());
        return content;
    }

    @Override
    public void handleGesture(Gesture gesture){
        if(gesture.equals(Gesture.SOUTH_NORTH)){
            windowMain.setWindow(windowMain.getHomeWindow());
        } else if(gesture.equals(Gesture.CENTER)){
            PlaceExecuter.place(currBlock.getBox().getPathto());
        } else if(gesture.equals(Gesture.WEST_EAST)){
            nextBlock();
        } else if(gesture.equals(Gesture.EAST_WEST)){
            lastBlock();
        }
    }

    private void nextBlock(){
        if((currBlockNum + 1) < blocks.size()){
            currBlockNum++;
            currBlock = blocks.get(currBlockNum);

            if(currBlockNum==(blocks.size()-1)){
                next=null;
            }

            if(currBlockNum > 0){
                last = lastDefault;
            }

            windowMain.updateActiveWindow();
        }

    }

    private void lastBlock(){
        if(currBlockNum > 0){
            currBlockNum--;
            currBlock = blocks.get(currBlockNum);

            if(currBlockNum == 0){
                last = null;
            }

            if(blocks.size() > 1){
                next = "Veeg naar rechts voor het volgende block";
            }

            windowMain.updateActiveWindow();
        }
    }

    @Override
    public boolean IsActive(){ return isActive; }

    @Override
    public void setActive(Boolean newActive) { isActive = newActive; }
}
