package LegoSortingBot.windows.windows;

import LegoSortingBot.enums.Gesture;
import LegoSortingBot.windows.WindowMain;
import LegoSortingBot.executers.GuideProcessor;

import javax.swing.*;
import java.awt.*;

public class GuideWindow extends Window {
    private boolean isActive = false;
    private WindowMain windowMain;
    private GuideProcessor guideProcessor;

    private String up;
    private String upDefault = "Veeg omhoog om verder te gaan met de vorige Handleiding";

    public GuideWindow(WindowMain windowMain){
        super("guide");
        this.windowMain = windowMain;
        guideProcessor = windowMain.getGuideProcessor();
    }

    @Override
    public JPanel getPanel(){
        JPanel content = new JPanel();
        GridLayout gridLayout = new GridLayout(11,3,0,0);
        content.setLayout(gridLayout);

        if(windowMain.getGuideProcessor().isLoaded()){
            up = upDefault;
        } else {
            up = null;
        }

        content.add(new JLabel());
        content.add(new JLabel(up));
        content.add(new JLabel());

        content.add(new JSeparator());
        content.add(new JSeparator());
        content.add(new JSeparator());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel("Veeg naar rechts om een handleiding te selecteren!"));
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JLabel());
        content.add(new JLabel());
        content.add(new JLabel());

        content.add(new JSeparator());
        content.add(new JSeparator());
        content.add(new JSeparator());

        content.add(new JLabel());
        content.add(new JLabel("Veeg omlaag om terug naar Home te gaan!"));
        content.add(new JLabel());
        return content;
    }

    @Override
    public void handleGesture(Gesture gesture){
        if(gesture.equals(Gesture.SOUTH_NORTH)){
            windowMain.setWindow(windowMain.getHomeWindow());
        } else if(gesture.equals(Gesture.WEST_EAST)){
            windowMain.getGuideProcessor().reset();
            windowMain.setWindow(windowMain.getGuideSelectorWindow());
        } else if(gesture.equals(Gesture.NORTH_SOUTH)){
            if(windowMain.getGuideProcessor().isLoaded()){
                windowMain.setWindow(windowMain.getGuideProcesWindow());
            }
        }
    }

    @Override
    public boolean IsActive(){ return isActive; }

    @Override
    public void setActive(Boolean newActive) { isActive = newActive; }
}
