package LegoSortingBot.windows;

import LegoSortingBot.enums.Gesture;
import LegoSortingBot.executers.GuideProcessor;
import LegoSortingBot.windows.windows.*;
import LegoSortingBot.windows.windows.Window;

import javax.swing.*;
import java.awt.*;

public class WindowMain {
    private Window activeWindow;
    private Window homeWindow;
    private Window manualWindow;
    private Window guideWindow;
    private Window sortWindow;
    private Window guideSelectorWindow;
    private Window guideProcesWindow;

    private GuideProcessor guideProcessor = new GuideProcessor(this);

    private JFrame frame;
    private JPanel mpanel;

    public WindowMain(){
        mpanel = new JPanel();

        homeWindow = new HomeWindow(this);
        manualWindow = new ManualWindow(this);
        guideWindow = new GuideWindow(this);
        sortWindow = new SortWindow(this);
        guideSelectorWindow = new GuideSelectorWindow(this);
        guideProcesWindow = new GuideProcesWindow(this);

        homeWindow.setActive(true);

        frame = new JFrame("Lego Sorting Bot");
        mpanel.add(homeWindow.getPanel());
        frame.add(mpanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        activeWindow = homeWindow;
    }

    public void setWindow(Window window){
        //set the window layout
        activeWindow.setActive(false);
        activeWindow = window;

        mpanel.removeAll();
        mpanel.add(activeWindow.getPanel());
        mpanel.revalidate();
        mpanel.repaint();

        activeWindow.setActive(true);
        System.out.println("Window updated to: " + activeWindow.getTitle());
    }

    public void updateActiveWindow(){
        mpanel.removeAll();
        mpanel.add(activeWindow.getPanel());
        mpanel.revalidate();
        mpanel.repaint();

        activeWindow.setActive(true);
        System.out.println("Acvite window updated!");
    }

    public Window getActiveWindow(){ return activeWindow; }
    public Window getHomeWindow(){ return homeWindow; }
    public Window getManualWindow(){ return manualWindow; }
    public Window getGuideWindow(){ return guideWindow; }
    public Window getSortWindow(){ return sortWindow; }
    public Window getGuideSelectorWindow(){ return guideSelectorWindow; }
    public Window getGuideProcesWindow() { return guideProcesWindow; }
    public JPanel getActivePanel(){ return mpanel; }
    public GuideProcessor getGuideProcessor() { return guideProcessor; }

}
