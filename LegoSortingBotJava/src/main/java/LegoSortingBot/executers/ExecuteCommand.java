package LegoSortingBot.executers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ExecuteCommand {

    public static void execute(String cmd) throws InterruptedException, IOException {
        Runtime run = Runtime.getRuntime();

        Process pr = run.exec(cmd);

        pr.waitFor();

        BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));

        String line = "";

        while ((line=buf.readLine())!=null) {

            System.out.println(line);

        }
    }

    public static void turnServo(int pin, int angle) throws InterruptedException, IOException {
        execute("python /home/pi/servoCommand.py " + pin + " " + angle);
    }
}
