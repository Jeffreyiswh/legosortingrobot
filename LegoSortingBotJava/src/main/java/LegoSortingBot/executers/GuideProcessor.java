package LegoSortingBot.executers;

import LegoSortingBot.enums.Block;
import LegoSortingBot.enums.Servo;
import LegoSortingBot.windows.WindowMain;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GuideProcessor {
    private List<List<Block>> stepList;
    private Integer nextStep = 1;
    private Integer currStep = 0;
    private WindowMain windowMain;
    private List<Block> currentStep;
    private String error;
    private boolean loaded = false;

    public GuideProcessor(WindowMain windowMain){
        stepList = new ArrayList<List<Block>>();
        this.windowMain = windowMain;
    }

    public void loadGuide(String file){
        FileReader fileReader = null;
        BufferedReader buffReader = null;
        try {
            fileReader = new FileReader(file);
            buffReader = new BufferedReader(fileReader);

            String currLine;
            while((currLine = buffReader.readLine()) != null){
                String[] blocks = currLine.split("[,]");
                List<Block> blockList = new ArrayList<Block>();

                for(String tag: blocks){ blockList.add(Block.getBlock(tag)); }
                stepList.add(blockList);
            }
            loaded = true;
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            try {
                if(buffReader != null){
                    buffReader.close();
                }

                if(fileReader != null){
                    fileReader.close();
                }
            } catch (IOException exept){
                exept.printStackTrace();
            }
        }
        currentStep = stepList.get(0);
        windowMain.setWindow(windowMain.getGuideProcesWindow());

        for(Block block : currentStep){
            Servo servoFrom = block.getBox().getServofrom();
            PlaceExecuter.drop(servoFrom);
        }
    }

    public void nextStep(){
        System.out.println("starting step " + currStep);
        if(nextStep == currStep + 1 && stepList.size() > currStep + 1){
            currStep++;
            nextStep++;
            currentStep = stepList.get(currStep);
            windowMain.updateActiveWindow();
            for(Block block : currentStep){
                Servo servoFrom = block.getBox().getServofrom();
                PlaceExecuter.drop(servoFrom);
            }
        }

        if(stepList.size() == currStep - 1){
            error = "Finished!";
        }

    }

    public void lastStep(){
        currStep--;
        currentStep = stepList.get(currStep);
        windowMain.updateActiveWindow();
    }

    public void reset(){
        nextStep = 1;
        currStep = 0;
        currentStep = null;
        error = null;
        loaded = false;
    }

    public List<Block> getCurrentStep() { return currentStep; }
    public Integer getNextStep() { return nextStep; }
    public String getError() { return error; }
    public Integer getCurrStep() { return currStep; }
    public boolean isLoaded() { return loaded; }
}
