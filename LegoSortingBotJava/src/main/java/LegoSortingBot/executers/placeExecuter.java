package LegoSortingBot.executers;

import LegoSortingBot.enums.Servo;

import java.io.IOException;

public class PlaceExecuter {
    public static void place(Servo[] path){
        try{
            for (Servo servo : path) {
                ExecuteCommand.turnServo(servo.getPin(), servo.getActiveangle());
            }

            Thread.sleep(1000 * 6);

            for (Servo servo: path) {
                ExecuteCommand.turnServo(servo.getPin(), servo.getSleepangle());
            }
        } catch(IOException ioe){
            ioe.printStackTrace();
        } catch (InterruptedException ie){
            ie.printStackTrace();
        }
    }

    public static void place(Servo servo){
        try{
            ExecuteCommand.turnServo(servo.getPin(), servo.getActiveangle());
            Thread.sleep(1000 * 6);
            ExecuteCommand.turnServo(servo.getPin(), servo.getSleepangle());
        } catch(IOException ioe){
            ioe.printStackTrace();
        } catch (InterruptedException ie){
            ie.printStackTrace();
        }
    }

    public static void drop(Servo servo){
        try{
            ExecuteCommand.turnServo(servo.getPin(), servo.getActiveangle());
            ExecuteCommand.turnServo(servo.getPin(), servo.getSleepangle());
        } catch(IOException ioe){
            ioe.printStackTrace();
        } catch (InterruptedException ie){
            ie.printStackTrace();
        }
    }

    public static void empty(Servo servo){
        try{
            ExecuteCommand.turnServo(servo.getPin(), servo.getActiveangle());
            Thread.sleep(1000 * 10);
            ExecuteCommand.turnServo(servo.getPin(), servo.getSleepangle());
        } catch(IOException ioe){
            ioe.printStackTrace();
        } catch (InterruptedException ie){
            ie.printStackTrace();
        }
    }
}
