#!/bin/python
 
# importeer de GPIO bibliotheek.
import RPi.GPIO as GPIO
# Importeer de time biblotheek voor tijdfuncties.
from time import sleep
import sys
 
# Zet de pinmode op Broadcom SOC.
GPIO.setmode(GPIO.BCM)
# Zet waarschuwingen uit.
GPIO.setwarnings(False)

nums = map(int, sys.argv[1:])
pin = nums[0]
angle = nums[1]

# Zet de GPIO pin als uitgang.
GPIO.setup(pin, GPIO.OUT)
# Configureer de pin voor PWM met een frequentie van 50Hz.
p = GPIO.PWM(pin, 50)
# Start PWM op de GPIO pin met een duty-cycle van 6%
p.start(5)

duty = float(angle) / 10.0 + 2.5
print duty 
p.ChangeDutyCycle(duty)
sleep(1)

p.stop()
GPIO.cleanup()
